# Brejinha - Hackathon AB InBev

## Disclaimer
Nosso grupo optou pela criação de um protótipo de alta fidelidade sem o desenvolvimento de código, já que acreditamos que durante a fase de validação e prototipação de uma ideia, o foco deve ser na qualidade, na rapidez de construção e em sua facilidade de ser escalonado. Após trabalhar os pontos críticos e essenciais do projeto, através de validação com stakeholders e possibilidades de incrementação, finalmente podemos desenvolvê-lo com código.
 
## Links

Video da demonstracao: [https://www.youtube.com/watch?v=UNCs-b3X2dU](https://www.youtube.com/watch?v=UNCs-b3X2dU)

Video do pitch: [https://www.youtube.com/watch?v=QXu7nExEA5Q](https://www.youtube.com/watch?v=QXu7nExEA5Q)

Prototipo: [https://www.figma.com/file/9FC8FvbqkmRaN4ROkOZ8gB/Hackathon-Secomp%2FAmbev?node-id=0%3A1](https://www.figma.com/file/9FC8FvbqkmRaN4ROkOZ8gB/Hackathon-Secomp%2FAmbev?node-id=0%3A1)

Demo: [https://www.figma.com/proto/9FC8FvbqkmRaN4ROkOZ8gB/Hackathon-Secomp-Ambev?node-id=25%3A0&scaling=scale-down](https://www.figma.com/proto/9FC8FvbqkmRaN4ROkOZ8gB/Hackathon-Secomp-Ambev?node-id=25%3A0&scaling=scale-down)